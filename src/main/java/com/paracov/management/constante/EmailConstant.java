package com.paracov.management.constante;

public class EmailConstant {
	public static final String SIMPLE_MAIL_TRANSFER_PROTOCOL = "smtps";
	public static final String USERNAME = "paracov.gmbh@gmail.com";
	public static final String PASSWORD = "1a2b3c4d*";
	public static final String FROM_EMAIL = "okheey.event@gmail.com";
	public static final String CC_EMAIL = "";
	public static final String EMAIL_SUBJECT = "PARACOV 2021, GmbH - Nouveau mot de passe";
	public static final String GMAIL_SMTP_SERVER = "smtp.gmail.com";
	public static final String SMTP_HOST = "mail.smtp.host";
	public static final String SMTP_AUTH = "mail.smtp.auth";
	public static final String SMTP_PORT = "mail.smtp.port";
	public static final int DEFAULT_PORT = 465;
	public static final String SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
	public static final String SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";

}
