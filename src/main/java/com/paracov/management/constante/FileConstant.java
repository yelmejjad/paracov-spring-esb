package com.paracov.management.constante;

public class FileConstant {
	
    public static final String USER_IMAGE_PATH = "/user/image/";
    public static final String JPG_EXTENSION = "jpg";
    public static final String USER_FOLDER = System.getProperty("user.home") + "/paracov/user/";
    public static final String DIRECTORY_CREATED = "Répertoire créé pour :  ";
    public static final String DEFAULT_USER_IMAGE_PATH = "/user/image/profile/";
    public static final String FILE_SAVED_IN_FILE_SYSTEM = "Fichier enregistré dans le système sous le nom :  ";
    public static final String DOT = ".";
    public static final String FORWARD_SLASH = "/";
    public static final String NOT_AN_IMAGE_FILE = " n'est pas une image. Veuillez sélectionner une image.";
    public static final String TEMP_PROFILE_IMAGE_BASE_URL = "https://robohash.org/";

}
