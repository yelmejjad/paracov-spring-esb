package com.paracov.management.constante;

public class SecurityConstant {
	
    public static final long EXPIRATION_TIME = 432_000_000; // nombre de jours en milliseconds -> 5 jours
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String JWT_TOKEN_HEADER = "Jwt-Token";
    public static final String TOKEN_CANNOT_BE_VERIFIED = "Token cannot be verified";
    public static final String PARACOV_GMBH = "PARACOV 2021, GmbH";
    public static final String PARACOV_ADMINISTRATION = "User Management Paracov";
    public static final String AUTHORITIES = "authorities";
    public static final String FORBIDDEN_MESSAGE = "Vous devez être connecté pour accéder à cette page ";
    public static final String ACCESS_DENIED_MESSAGE = "Vous n'avez pas la permission d'accéder à cette page";
    public static final String OPTIONS_HTTP_METHOD = "OPTIONS";
    public static final String[] PUBLIC_URLS = { "/user/login", "/user/register", "/user/image/**" };

}
