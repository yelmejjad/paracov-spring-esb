package com.paracov.management.constante;

public class UserImplementationConstant {
	
    public static final String USERNAME_ALREADY_EXISTS = "Ce nom d'utilisateur existe déjà";
    public static final String EMAIL_ALREADY_EXISTS = "Cette adresse email existe déjà";
    public static final String NO_USER_FOUND_BY_USERNAME = "Aucun utilisateur n'existe sous le nom d'utilisateur suivant : ";
    public static final String FOUND_USER_BY_USERNAME = "Utilisateur trouvé sous le nom d'utilisateur suivant : ";
    public static final String NO_USER_FOUND_BY_EMAIL = "Aucun utilisateur trouvé pour l'adresse mail : ";

}
