package com.paracov.management.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paracov.management.modele.User;

public interface IUserRepository extends JpaRepository<User, Long> {

	User findUserByUsername(String username);

	User findUserByEmail(String email);

}
