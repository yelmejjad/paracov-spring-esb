package com.paracov.management.enume;

import static com.paracov.management.constante.Authority.ADMIN_AUTHORITIES;
import static com.paracov.management.constante.Authority.MANAGER_AUTHORITIES;
import static com.paracov.management.constante.Authority.SUPER_ADMIN_AUTHORITIES;
import static com.paracov.management.constante.Authority.USER_AUTHORITIES;

public enum Role {
	ROLE_USER(USER_AUTHORITIES), ROLE_MANAGER(MANAGER_AUTHORITIES), ROLE_ADMIN(ADMIN_AUTHORITIES),
	ROLE_SUPER_ADMIN(SUPER_ADMIN_AUTHORITIES);

	private String[] authorities;

	Role(String... authorities) {
		this.authorities = authorities;
	}

	public String[] getAuthorities() {
		return authorities;
	}

}
