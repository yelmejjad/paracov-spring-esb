package com.paracov.management.exception.modele;

public class EmailNotFoundException extends Exception {

	public EmailNotFoundException(String message) {
		super(message);
	}
	
}

