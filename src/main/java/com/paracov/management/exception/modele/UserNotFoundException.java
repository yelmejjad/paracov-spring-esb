package com.paracov.management.exception.modele;

public class UserNotFoundException extends Exception {

	public UserNotFoundException(String message) {
		super(message);
	}
	
	

}

